SELECT
    v.viewer_id AS id,
    uuta.active,
    v.uname,
    v.anonymous,
    v.random_id,
    u.creation_date,
    u.postal_code,
    (
        "replace"(
            (u.postal_code) :: text,
            ('-' :: character varying) :: text,
            ('' :: character varying) :: text
        )
    ) :: integer AS formatted_postal_code,
    CASE
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 19999999
        ) THEN 'SP' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 28999999
        ) THEN 'RJ' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 29999999
        ) THEN 'ES' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 39999999
        ) THEN 'MG' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 48999999
        ) THEN 'BA' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 49999999
        ) THEN 'SE' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 56999999
        ) THEN 'PE' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 57999999
        ) THEN 'AL' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 58999999
        ) THEN 'PB' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 59999999
        ) THEN 'RN' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 63999999
        ) THEN 'CE' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 64999999
        ) THEN 'PI' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 65999999
        ) THEN 'MA' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68899999
        ) THEN 'PA' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68999999
        ) THEN 'AP' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69299999
        ) THEN 'AM' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69399999
        ) THEN 'RR' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69899999
        ) THEN 'AM' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69999999
        ) THEN 'AC' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72799999
        ) THEN 'DF' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72999999
        ) THEN 'GO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 73699999
        ) THEN 'DF' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76799999
        ) THEN 'GO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76999999
        ) THEN 'RO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 77999999
        ) THEN 'TO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 78899999
        ) THEN 'MT' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 79999999
        ) THEN 'MS' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 87999999
        ) THEN 'PR' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 89999999
        ) THEN 'SC' :: character varying
        ELSE 'RS' :: character varying
    END AS uf,
    CASE
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 19999999
        ) THEN 'São Paulo' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 28999999
        ) THEN 'Rio de Janeiro' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 29999999
        ) THEN 'Espírito Santo' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 39999999
        ) THEN 'Minas Gerais' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 48999999
        ) THEN 'Bahia' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 49999999
        ) THEN 'Sergipe' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 56999999
        ) THEN 'Pernambuco' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 57999999
        ) THEN 'Alagoas' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 58999999
        ) THEN 'Paraíba' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 59999999
        ) THEN 'Rio Grande do Norte' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 63999999
        ) THEN 'Ceará' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 64999999
        ) THEN 'Piauí' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 65999999
        ) THEN 'Maranhão' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68899999
        ) THEN 'Pará' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68999999
        ) THEN 'Amapá' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69299999
        ) THEN 'Amazonas' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69399999
        ) THEN 'Roraima' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69899999
        ) THEN 'Amazonas' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69999999
        ) THEN 'Acre' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72799999
        ) THEN 'Distrito Federal' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72999999
        ) THEN 'Goiás' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 73699999
        ) THEN 'Distrito Federal' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76799999
        ) THEN 'Goiás' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76999999
        ) THEN 'Rondônia' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 77999999
        ) THEN 'Tocantins' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 78899999
        ) THEN 'Mato Grosso' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 79999999
        ) THEN 'Mato Grosso do Sul' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 87999999
        ) THEN 'Paraná' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 89999999
        ) THEN 'Santa Catarina' :: character varying
        ELSE 'Rio Grande do Sul' :: character varying
    END AS state,
    u.gender,
    u.birth_date,
    s.subscription_id,
    v.distributor_distributorid,
    CASE
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 6
        ) THEN 'menos de 6 meses' :: character varying
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 12
        ) THEN 'de 6 meses a 1 ano' :: character varying
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 24
        ) THEN 'de 1 a 2 anos' :: character varying
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 'de 2 a 5 anos' :: character varying
        ELSE 'mais que 5 anos' :: character varying
    END AS acquisition_duration,
    CASE
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 6
        ) THEN 0
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 12
        ) THEN 1
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 24
        ) THEN 2
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 3
        ELSE 4
    END AS acquisition_duration_id,
    CASE
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 0
        ) THEN 'não informado' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 18
        ) THEN 'menor de 18 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 25
        ) THEN 'de 18 a 25 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 30
        ) THEN 'de 25 a 30 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 40
        ) THEN 'de 30 a 40 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 50
        ) THEN 'de 40 a 50 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 'de 50 a 60 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 200
        ) THEN 'acima de 60 anos' :: character varying
        ELSE 'não informado' :: character varying
    END AS age_text,
    CASE
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 18
        ) THEN 0
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 25
        ) THEN 1
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 30
        ) THEN 2
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 40
        ) THEN 3
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 50
        ) THEN 4
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 5
        ELSE 6
    END AS age_text_id,
    date_diff(
        ('year' :: character varying) :: text,
        u.birth_date,
        (('now' :: character varying) :: date) :: timestamp without time zone
    ) AS age
FROM
    (
        (
            (
                postgres_producao.viewer v
                JOIN postgres_producao."user" u ON (((u.login) :: text = (v.uname) :: text))
            )
            JOIN postgres_producao.user_user_type_association uuta ON (
                (
                    (uuta.user_userid = u.user_id)
                    AND (
                        uuta.distributor_id = v.distributor_distributorid
                    )
                )
            )
        )
        JOIN postgres_producao.subscription s ON (((s.user_id) :: text = (v.random_id) :: text))
    );