SELECT
    hb.id,
    hb.creation_date,
    dm.last_access_date,
    hb.duration,
    hb.device_id,
    hb.transaction_type,
    hb.content_id,
    dm.device_platform,
    CASE
        WHEN (dm.device_type < 2) THEN 'SmartTV/Box' :: character varying
        WHEN (dm.device_type = 2) THEN 'Android' :: character varying
        WHEN (dm.device_type = 3) THEN 'Android' :: character varying
        WHEN (dm.device_type = 4) THEN 'iOS' :: character varying
        WHEN (dm.device_type = 5) THEN 'iOS' :: character varying
        ELSE 'Browser' :: character varying
    END AS device_name,
    v.viewer_id
FROM
    (
        (
            postgres_producao.acc_heart_beat hb
            JOIN postgres_producao.viewer v ON (((v.random_id) :: text = (hb.user_id) :: text))
        )
        JOIN postgres_producao.device_management dm ON (
            (
                (hb.device_id) :: text = (dm.device_management_id) :: text
            )
        )
    );