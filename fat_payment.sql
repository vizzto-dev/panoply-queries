SELECT
    t.ticket_id AS payment_id,
    t.totals,
    t.status,
    t.created,
    t.service_id,
    (
        to_char(t.created, ('YYYYMMDD' :: character varying) :: text)
    ) :: character varying AS dt_id,
    CASE
        WHEN (t.service_id = 2) THEN b.subscription_subscriptionid
        ELSE NULL :: bigint
    END AS subscription_id,
    CASE
        WHEN (t.service_id = 2) THEN t.product_id
        ELSE NULL :: bigint
    END AS plan_id,
    CASE
        WHEN (t.service_id = 4) THEN t.product_id
        ELSE NULL :: bigint
    END AS content_id,
    v.viewer_id,
    v.distributor_distributorid
FROM
    (
        (
            (
                (
                    postgres_producao.ticket t
                    JOIN postgres_producao.puser p ON ((p.user_id = t.user_userid))
                )
                JOIN postgres_producao.viewer v ON (((v.uname) :: text = (p.uname) :: text))
            )
            JOIN postgres_producao.broker_ticket bt ON ((bt.payments_ticket_id = t.ticket_id))
        )
        JOIN postgres_producao.bill b ON ((b.bill_id = bt.external_ticket_id))
    )
UNION
    (
        SELECT
            NULL :: "unknown" AS payment_id,
            0 AS totals,
            2 AS status,
            s."start" AS created,
            2 AS service_id,
            (
                to_char(s."start", ('YYYYMMDD' :: character varying) :: text)
            ) :: character varying AS dt_id,
            s.subscription_id,
            s.plan_planid AS plan_id,
            NULL :: "unknown" AS content_id,
            v.viewer_id,
            v.distributor_distributorid
        FROM
            (
                postgres_producao.subscription s
                JOIN postgres_producao.viewer v ON (((v.uname) :: text = (s.uname) :: text))
            )
        WHERE
            (
                NOT (
                    s.plan_planid IN (
                        SELECT
                            DISTINCT t.product_id
                        FROM
                            postgres_producao.ticket t
                        WHERE
                            (t.service_id = 2)
                    )
                )
            )
        ORDER BY
            s.plan_planid
    );