SELECT
    catalog_content_distributor_channel.id,
    catalog_content_distributor_channel.catalog_content_distributor_id,
    catalog_content_distributor_channel.channel_id
FROM
    postgres_producao.catalog_content_distributor_channel;