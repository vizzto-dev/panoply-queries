SELECT
    catalog_content_distributor_category.id,
    catalog_content_distributor_category.catalog_content_distributor_id,
    catalog_content_distributor_category.category_id
FROM
    postgres_producao.catalog_content_distributor_category;