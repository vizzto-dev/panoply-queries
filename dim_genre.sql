SELECT
    g.id,
    g.genre_type,
    lt.value AS genre_name,
    cg.content_id
FROM
    (
        (
            (
                postgres_producao.genre g
                JOIN postgres_producao.content_genre cg ON ((cg.genre_id = g.id))
            )
            JOIN postgres_producao.genre_text gt ON ((gt.genre_id = g.id))
        )
        JOIN postgres_producao.localized_text lt ON ((lt.id = gt.genre_value_id))
    );