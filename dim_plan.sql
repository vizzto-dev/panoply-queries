SELECT
    p.plan_id AS id,
    p.cost,
    p.name,
    p.open_for_subscriptions,
    p.plan_owner_id AS distributor_id
FROM
    postgres_producao.plan p;