SELECT
    vsp_user_session.id,
    vsp_user_session.uname,
    vsp_user_session.session_start,
    CASE
        WHEN ((vsp_user_session.device_type) :: text < (2) :: text) THEN 'SmartTV/Box' :: character varying
        WHEN ((vsp_user_session.device_type) :: text = (2) :: text) THEN 'Smartphone' :: character varying
        WHEN ((vsp_user_session.device_type) :: text = (3) :: text) THEN 'Tablet' :: character varying
        WHEN ((vsp_user_session.device_type) :: text = (4) :: text) THEN 'Smartphone' :: character varying
        WHEN ((vsp_user_session.device_type) :: text = (5) :: text) THEN 'Tablet' :: character varying
        ELSE 'Browser' :: character varying
    END AS device
FROM
    postgres_producao.vsp_user_session;