SELECT
    ccd.id,
    cc.content_id,
    cd.distributor_id,
    cd.catalog_id,
    CASE
        WHEN (ccd.catalog_content_distributor_status = 0) THEN 'Em edi\303\247\303\243o' :: character varying
        WHEN (ccd.catalog_content_distributor_status = 1) THEN 'Public\303\241vel' :: character varying
        WHEN (ccd.catalog_content_distributor_status = 2) THEN 'Publicado' :: character varying
        ELSE NULL :: character varying
    END AS status
FROM
    (
        (
            postgres_producao.catalog_content_distributor ccd
            JOIN postgres_producao.catalog_content cc ON ((cc.id = ccd.catalog_content_id))
        )
        JOIN postgres_producao.catalog_distributor cd ON ((cd.id = ccd.catalog_distributor_id))
    );