SELECT
    p.provider_id AS id,
    u.name
FROM
    (
        postgres_producao.provider p
        JOIN postgres_producao."user" u ON (((u.login) :: text = (p.uname) :: text))
    );