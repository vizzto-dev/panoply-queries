SELECT
    s.subscription_id,
    s.cancel_date,
    CASE
        WHEN ((s.status) :: text = 'CANCELED' :: text) THEN true
        ELSE false
    END AS canceled,
    s.plan_plan_id AS plan_id,
    s."start",
    v.viewer_id AS user_id
FROM
    (
        postgres_producao.subscription s
        JOIN postgres_producao.viewer v ON (((v.random_id) :: text = (s.user_id) :: text))
    )
WHERE
    (
        s.subscription_id IN (
            SELECT
                "max"(subscription.subscription_id) AS "max"
            FROM
                postgres_producao.subscription subscription
            GROUP BY
                subscription.user_id,
                subscription.cancel_date
        )
    );