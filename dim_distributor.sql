SELECT
    d.distributor_id AS id,
    d.alias,
    d.name
FROM
    postgres_producao.distributor d;