SELECT
    catalog_content_distributor_country.id,
    catalog_content_distributor_country.catalog_content_distributor_id,
    CASE
        WHEN (
            catalog_content_distributor_country.rent_price IS NULL
        ) THEN NULL :: text
        WHEN (
            catalog_content_distributor_country.rent_price < (6) :: numeric(38, 6)
        ) THEN '0 a 6' :: text
        WHEN (
            catalog_content_distributor_country.rent_price < (9) :: numeric(38, 6)
        ) THEN '6 a 9' :: text
        WHEN (
            catalog_content_distributor_country.rent_price < (12) :: numeric(38, 6)
        ) THEN '9 a 12' :: text
        WHEN (
            catalog_content_distributor_country.rent_price < (19) :: numeric(38, 6)
        ) THEN '12 a 19' :: text
        WHEN (
            catalog_content_distributor_country.rent_price < (30) :: numeric(38, 6)
        ) THEN '19 a 30' :: text
        WHEN (
            catalog_content_distributor_country.rent_price < (50) :: numeric(38, 6)
        ) THEN '30 a 50' :: text
        ELSE 'Acima de 50' :: text
    END AS rent_price
FROM
    postgres_producao.catalog_content_distributor_country;