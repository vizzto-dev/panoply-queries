SELECT
    h.id AS access_id,
    h.creation_date,
    h.duration,
    h.start_index,
    h.stop_index,
    v.viewer_id,
    h.content_id
FROM
    (
        postgres_producao.acc_heart_beat h
        JOIN postgres_producao.viewer v ON (((v.random_id) :: text = (h.user_id) :: text))
    );