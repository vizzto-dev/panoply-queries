SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    null as session_id,
    null as session_uname,
    null as session_start,
    null AS session_device,
    null AS plan_id,
    null AS plan_price,
    null as plan_name,
    null as plan_active,
    null as subscription_id,
    null as subscription_cancel_date,
    null AS subscription_canceled,
    null as subscription_start,
    null as heartbeat_id,
    null as heartbeat_date,
    null as device_last_access,
    null as heartbeat_duration,
    null as device_id,
    null as heartbeat_transaction_type,
    null as heartbeat_content_id,
    null as device_platform,
    null as device_name,
    null as viewer_id,
    null as user_active,
    null as user_uname,
    null as user_anonymous,
    null as viewer_randon_id,
    null as user_creation_date,
    null as user_postal_code,
    null AS user_formatted_postal_code,
    null AS user_uf,
    null AS user_state,
    null as user_gender,
    null as user_birthday,
    null as user_acquisition_duration,
    null as user_acquisition_duration_id,
    null as user_age_text,
    null as user_age_text_id,
    null as user_age,
    null AS payment_id,
    null as payment_totals,
    null AS payment_status,
    null as payment_date,
    null AS content_id
FROM
    postgres_producao.distributor d
UNION
SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    vus.id as session_id,
    vus.uname as session_uname,
    vus.session_start as session_start,
    CASE
        WHEN ((vus.device_type) :: text < (2) :: text) THEN 'SmartTV/Box' :: character varying
        WHEN ((vus.device_type) :: text = (2) :: text) THEN 'Smartphone' :: character varying
        WHEN ((vus.device_type) :: text = (3) :: text) THEN 'Tablet' :: character varying
        WHEN ((vus.device_type) :: text = (4) :: text) THEN 'Smartphone' :: character varying
        WHEN ((vus.device_type) :: text = (5) :: text) THEN 'Tablet' :: character varying
        ELSE 'Browser' :: character varying
    END AS session_device,
    null AS plan_id,
    null AS plan_price,
    null as plan_name,
    null as plan_active,
    null as subscription_id,
    null as subscription_cancel_date,
    null AS subscription_canceled,
    null as subscription_start,
    null as heartbeat_id,
    null as heartbeat_date,
    null as device_last_access,
    null as heartbeat_duration,
    null as device_id,
    null as heartbeat_transaction_type,
    null as heartbeat_content_id,
    null as device_platform,
    null as device_name,
    null as viewer_id,
    null as user_active,
    null as user_uname,
    null as user_anonymous,
    null as viewer_randon_id,
    null as user_creation_date,
    null as user_postal_code,
    null AS user_formatted_postal_code,
    null AS user_uf,
    null AS user_state,
    null as user_gender,
    null as user_birthday,
    null as user_acquisition_duration,
    null as user_acquisition_duration_id,
    null as user_age_text,
    null as user_age_text_id,
    null as user_age,
    null AS payment_id,
    null as payment_totals,
    null AS payment_status,
    null as payment_date,
    null AS content_id
FROM postgres_producao.vsp_user_session vus
INNER JOIN postgres_producao.distributor d ON vus.distributor_id = d.distributor_id
UNION
SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    null as session_id,
    null as session_uname,
    null as session_start,
    null AS session_device,
    plan.plan_id AS plan_id,
    plan.cost AS plan_price,
    plan.name as plan_name,
    plan.open_for_subscriptions as plan_active,
    null as subscription_id,
    null as subscription_cancel_date,
    null AS subscription_canceled,
    null as subscription_start,
    null as heartbeat_id,
    null as heartbeat_date,
    null as device_last_access,
    null as heartbeat_duration,
    null as device_id,
    null as heartbeat_transaction_type,
    null as heartbeat_content_id,
    null as device_platform,
    null as device_name,
    null as viewer_id,
    null as user_active,
    null as user_uname,
    null as user_anonymous,
    null as viewer_randon_id,
    null as user_creation_date,
    null as user_postal_code,
    null AS user_formatted_postal_code,
    null AS user_uf,
    null AS user_state,
    null as user_gender,
    null as user_birthday,
    null as user_acquisition_duration,
    null as user_acquisition_duration_id,
    null as user_age_text,
    null as user_age_text_id,
    null as user_age,
    null AS payment_id,
    null as payment_totals,
    null AS payment_status,
    null as payment_date,
    null AS content_id
FROM postgres_producao.plan plan
INNER JOIN postgres_producao.distributor d ON plan.plan_owner_id = d.distributor_id
UNION
SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    null as session_id,
    null as session_uname,
    null as session_start,
    null AS session_device,
    plan.plan_id AS plan_id,
    plan.cost AS plan_price,
    plan.name as plan_name,
    plan.open_for_subscriptions as plan_active,
    subs.subscription_id as subscription_id,
    subs.cancel_date as subscription_cancel_date,
    CASE
        WHEN ((subs.status) :: text = 'CANCELED' :: text) THEN true
        ELSE false
    END AS subscription_canceled,
    subs."start" as subscription_start,
    null as heartbeat_id,
    null as heartbeat_date,
    null as device_last_access,
    null as heartbeat_duration,
    null as device_id,
    null as heartbeat_transaction_type,
    null as heartbeat_content_id,
    null as device_platform,
    null as device_name,
    null as viewer_id,
    null as user_active,
    null as user_uname,
    null as user_anonymous,
    null as viewer_randon_id,
    null as user_creation_date,
    null as user_postal_code,
    null AS user_formatted_postal_code,
    null AS user_uf,
    null AS user_state,
    null as user_gender,
    null as user_birthday,
    null as user_acquisition_duration,
    null as user_acquisition_duration_id,
    null as user_age_text,
    null as user_age_text_id,
    null as user_age,
    null AS payment_id,
    null as payment_totals,
    null AS payment_status,
    null as payment_date,
    null AS content_id
FROM postgres_producao.subscription subs
INNER JOIN postgres_producao.plan plan ON subs.plan_plan_id = plan.plan_id
INNER JOIN postgres_producao.distributor d ON plan.plan_owner_id = d.distributor_id
UNION
SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    null as session_id,
    null as session_uname,
    null as session_start,
    null AS session_device,
    plan.plan_id AS plan_id,
    plan.cost AS plan_price,
    plan.name as plan_name,
    plan.open_for_subscriptions as plan_active,
    subs.subscription_id as subscription_id,
    subs.cancel_date as subscription_cancel_date,
    CASE
        WHEN ((subs.status) :: text = 'CANCELED' :: text) THEN true
        ELSE false
    END AS subscription_canceled,
    subs."start" as subscription_start,
    null as heartbeat_id,
    null as heartbeat_date,
    null as device_last_access,
    null as heartbeat_duration,
    null as device_id,
    null as heartbeat_transaction_type,
    null as heartbeat_content_id,
    null as device_platform,
    null as device_name,
    v.viewer_id as viewer_id,
    uuta.active as user_active,
    v.uname as user_uname,
    v.anonymous as user_anonymous,
    v.random_id as viewer_randon_id,
    u.creation_date as user_creation_date,
    u.postal_code as user_postal_code,
    (
        "replace"(
            (u.postal_code) :: text,
            ('-' :: character varying) :: text,
            ('' :: character varying) :: text
        )
    ) :: integer AS user_formatted_postal_code,
    CASE
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 19999999
        ) THEN 'SP' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 28999999
        ) THEN 'RJ' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 29999999
        ) THEN 'ES' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 39999999
        ) THEN 'MG' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 48999999
        ) THEN 'BA' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 49999999
        ) THEN 'SE' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 56999999
        ) THEN 'PE' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 57999999
        ) THEN 'AL' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 58999999
        ) THEN 'PB' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 59999999
        ) THEN 'RN' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 63999999
        ) THEN 'CE' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 64999999
        ) THEN 'PI' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 65999999
        ) THEN 'MA' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68899999
        ) THEN 'PA' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68999999
        ) THEN 'AP' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69299999
        ) THEN 'AM' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69399999
        ) THEN 'RR' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69899999
        ) THEN 'AM' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69999999
        ) THEN 'AC' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72799999
        ) THEN 'DF' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72999999
        ) THEN 'GO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 73699999
        ) THEN 'DF' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76799999
        ) THEN 'GO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76999999
        ) THEN 'RO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 77999999
        ) THEN 'TO' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 78899999
        ) THEN 'MT' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 79999999
        ) THEN 'MS' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 87999999
        ) THEN 'PR' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 89999999
        ) THEN 'SC' :: character varying
        ELSE 'RS' :: character varying
    END AS user_uf,
    CASE
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 19999999
        ) THEN 'São Paulo' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 28999999
        ) THEN 'Rio de Janeiro' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 29999999
        ) THEN 'Espírito Santo' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 39999999
        ) THEN 'Minas Gerais' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 48999999
        ) THEN 'Bahia' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 49999999
        ) THEN 'Sergipe' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 56999999
        ) THEN 'Pernambuco' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 57999999
        ) THEN 'Alagoas' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 58999999
        ) THEN 'Paraíba' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 59999999
        ) THEN 'Rio Grande do Norte' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 63999999
        ) THEN 'Ceará' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 64999999
        ) THEN 'Piauí' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 65999999
        ) THEN 'Maranhão' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68899999
        ) THEN 'Pará' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 68999999
        ) THEN 'Amapá' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69299999
        ) THEN 'Amazonas' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69399999
        ) THEN 'Roraima' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69899999
        ) THEN 'Amazonas' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 69999999
        ) THEN 'Acre' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72799999
        ) THEN 'Distrito Federal' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 72999999
        ) THEN 'Goiás' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 73699999
        ) THEN 'Distrito Federal' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76799999
        ) THEN 'Goiás' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 76999999
        ) THEN 'Rondônia' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 77999999
        ) THEN 'Tocantins' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 78899999
        ) THEN 'Mato Grosso' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 79999999
        ) THEN 'Mato Grosso do Sul' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 87999999
        ) THEN 'Paraná' :: character varying
        WHEN (
            (
                "replace"(
                    (u.postal_code) :: text,
                    ('-' :: character varying) :: text,
                    ('' :: character varying) :: text
                )
            ) :: integer < 89999999
        ) THEN 'Santa Catarina' :: character varying
        ELSE 'Rio Grande do Sul' :: character varying
    END AS user_state,
    u.gender as user_gender,
    u.birth_date as user_birthday,
    CASE
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 6
        ) THEN 'menos de 6 meses' :: character varying
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 12
        ) THEN 'de 6 meses a 1 ano' :: character varying
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 24
        ) THEN 'de 1 a 2 anos' :: character varying
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 'de 2 a 5 anos' :: character varying
        ELSE 'mais que 5 anos' :: character varying
    END as user_acquisition_duration,
    CASE
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 6
        ) THEN 0
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 12
        ) THEN 1
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 24
        ) THEN 2
        WHEN (
            date_diff(
                ('month' :: character varying) :: text,
                u.creation_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 3
        ELSE 4
    END as user_acquisition_duration_id,
    CASE
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 18
        ) THEN 'menor de 18 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 25
        ) THEN 'de 18 a 25 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 30
        ) THEN 'de 25 a 30 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 40
        ) THEN 'de 30 a 40 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 50
        ) THEN 'de 40 a 50 anos' :: character varying
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 'de 50 a 60 anos' :: character varying
        ELSE 'acima de 60 anos' :: character varying
    END as user_age_text,
    CASE
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 18
        ) THEN 0
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 25
        ) THEN 1
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 30
        ) THEN 2
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 40
        ) THEN 3
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 50
        ) THEN 4
        WHEN (
            date_diff(
                ('year' :: character varying) :: text,
                u.birth_date,
                (('now' :: character varying) :: date) :: timestamp without time zone
            ) <= 60
        ) THEN 5
        ELSE 6
    END as user_age_text_id,
    date_diff(
        ('year' :: character varying) :: text,
        u.birth_date,
        (('now' :: character varying) :: date) :: timestamp without time zone
    ) as user_age,
    null AS payment_id,
    null as payment_totals,
    null AS payment_status,
    null as payment_date,
    null AS content_id
FROM postgres_producao.viewer v
INNER JOIN postgres_producao."user" u ON u.login = v.uname
INNER JOIN postgres_producao.user_user_type_association uuta ON uuta.user_userid = u.user_id
INNER JOIN postgres_producao.subscription subs ON subs.user_id = v.random_id
INNER JOIN postgres_producao.plan ON subs.plan_plan_id = plan.plan_id
INNER JOIN postgres_producao.distributor d ON v.distributor_distributorid = d.distributor_id
UNION
SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    null as session_id,
    null as session_uname,
    null as session_start,
    null AS session_device,
    null AS plan_id,
    null AS plan_price,
    null as plan_name,
    null as plan_active,
    null as subscription_id,
    null as subscription_cancel_date,
    null AS subscription_canceled,
    null as subscription_start,
    hb.id as heartbeat_id,
    hb.creation_date as heartbeat_date,
    dm.last_access_date as device_last_access,
    hb.duration as heartbeat_duration,
    hb.device_id as device_id,
    hb.transaction_type as heartbeat_transaction_type,
    hb.content_id as heartbeat_content_id,
    dm.device_platform as device_platform,
    CASE
        WHEN (dm."type" < 2) THEN 'SmartTV/Box' :: character varying
        WHEN (dm."type" = 2) THEN 'Android' :: character varying
        WHEN (dm."type" = 3) THEN 'Android' :: character varying
        WHEN (dm."type" = 4) THEN 'iOS' :: character varying
        WHEN (dm."type" = 5) THEN 'iOS' :: character varying
        ELSE 'Browser' :: character varying
    END AS device_name,
    null as viewer_id,
    null as user_active,
    null as user_uname,
    null as user_anonymous,
    null as viewer_randon_id,
    null as user_creation_date,
    null as user_postal_code,
    null AS user_formatted_postal_code,
    null AS user_uf,
    null AS user_state,
    null as user_gender,
    null as user_birthday,
    null as user_acquisition_duration,
    null as user_acquisition_duration_id,
    null as user_age_text,
    null as user_age_text_id,
    null as user_age,
    null AS payment_id,
    null as payment_totals,
    null AS payment_status,
    null as payment_date,
    null AS content_id
FROM postgres_producao.acc_heart_beat hb
INNER JOIN postgres_producao.viewer v ON v.random_id = hb.user_id
INNER JOIN postgres_producao.device_management dm ON hb.device_id = dm.device_management_id
INNER JOIN postgres_producao.distributor d ON d.distributor_id = v.distributor_distributorid
UNION

SELECT
    d.distributor_id as distributor_id,
    d.alias as distributor_alias,
    d.name as distributor_name,
    null as session_id,
    null as session_uname,
    null as session_start,
    null AS session_device,
    CASE
        WHEN (t.service_id = 2) THEN plan.plan_id
        ELSE NULL :: bigint
    END AS plan_id,
    CASE
        WHEN (t.service_id = 2) THEN plan.cost
        ELSE NULL
    END AS plan_price,
    CASE
        WHEN (t.service_id = 2) THEN plan.name
        ELSE NULL
    END AS plan_name,
    CASE
        WHEN (t.service_id = 2) THEN plan.open_for_subscriptions
        ELSE NULL
    END AS plan_active,
    CASE
        WHEN (t.service_id = 2) THEN s.subscription_id
        ELSE NULL
    END AS subscription_id,
    CASE
        WHEN (t.service_id = 2) THEN s.cancel_date
        ELSE NULL
    END AS subscription_cancel_date,
    null AS subscription_canceled,
    CASE
        WHEN (t.service_id = 2) THEN s."start"
        ELSE NULL
    END AS subscription_start,
    null as heartbeat_id,
    null as heartbeat_date,
    null as device_last_access,
    null as heartbeat_duration,
    null as device_id,
    null as heartbeat_transaction_type,
    null as heartbeat_content_id,
    null as device_platform,
    null as device_name,
    null as viewer_id,
    null as user_active,
    null as user_uname,
    null as user_anonymous,
    null as viewer_randon_id,
    null as user_creation_date,
    null as user_postal_code,
    null AS user_formatted_postal_code,
    null AS user_uf,
    null AS user_state,
    null as user_gender,
    null as user_birthday,
    null as user_acquisition_duration,
    null as user_acquisition_duration_id,
    null as user_age_text,
    null as user_age_text_id,
    null as user_age,
    t.ticket_id AS payment_id,
    t.totals as payment_totals,
    CASE
        WHEN (
            (t.ticket_status) :: text = ('OPEN' :: character varying) :: text
        ) THEN 1
        WHEN (
            (t.ticket_status) :: text = ('PAID' :: character varying) :: text
        ) THEN 2
        WHEN (
            (t.ticket_status) :: text = ('CANCELLED' :: character varying) :: text
        ) THEN 4
        WHEN (
            (t.ticket_status) :: text = ('CONTESTED' :: character varying) :: text
        ) THEN 6
        ELSE NULL
    END AS payment_status,
    t.created as payment_date,
    CASE
        WHEN (t.service_id = 4) THEN t.product_id
        ELSE NULL
    END AS content_id
FROM postgres_producao.ticket t
INNER JOIN postgres_producao.payment_user p ON p.id = t.user_id
INNER JOIN postgres_producao.viewer v ON v.random_id = p.user_id
INNER JOIN postgres_producao.distributor d ON v.distributor_distributorid = d.distributor_id
INNER JOIN postgres_producao.plan plan ON plan.plan_id = t.product_id
LEFT JOIN postgres_producao.bill b ON b.bill_id = t.service_bill_id
INNER JOIN postgres_producao.subscription s ON s.subscription_id = b.subscription_subscription_id

