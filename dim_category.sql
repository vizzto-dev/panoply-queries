SELECT
    category.category_id,
    category.distributor_distributorid AS distributor_id,
    category.name
FROM
    postgres_producao.category;