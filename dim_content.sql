SELECT
    c.id,
    CASE
        WHEN (c.content_type = 0) THEN 'Vídeo' :: character varying
        WHEN (c.content_type = 1) THEN 'Filme' :: character varying
        WHEN (c.content_type = 2) THEN 'Série' :: character varying
        WHEN (c.content_type = 3) THEN 'Série' :: character varying
        WHEN (c.content_type = 4) THEN 'Temporada' :: character varying
        WHEN (c.content_type = 5) THEN 'Episódio' :: character varying
        WHEN (c.content_type = 6) THEN 'Coleção' :: character varying
        WHEN (c.content_type = 7) THEN 'Vídeo ao Vivo' :: character varying
        WHEN (c.content_type = 8) THEN 'Vídeo Gravado' :: character varying
        WHEN (c.content_type = 9) THEN 'Evento Esportivo' :: character varying
        WHEN (c.content_type = 10) THEN 'Show' :: character varying
        WHEN (c.content_type = 11) THEN 'Áudio' :: character varying
        WHEN (c.content_type = 12) THEN 'Música' :: character varying
        WHEN (c.content_type = 13) THEN 'Álbum' :: character varying
        WHEN (c.content_type = 14) THEN 'Áudio ao Vivo' :: character varying
        WHEN (c.content_type = 15) THEN 'Áudio Gravado' :: character varying
        WHEN (c.content_type = 16) THEN 'Curso' :: character varying
        WHEN (c.content_type = 17) THEN 'Módulo' :: character varying
        WHEN (c.content_type = 18) THEN 'Aula' :: character varying
        WHEN (c.content_type = 19) THEN 'Playlist' :: character varying
        WHEN (c.content_type = 20) THEN 'Seção' :: character varying
        WHEN (c.content_type = 21) THEN 'Videoaula' :: character varying
        WHEN (c.content_type = 22) THEN 'Palestra' :: character varying
        ELSE NULL :: character varying
    END AS content_type,
    c.original_name AS name,
    CASE
        WHEN (c.content_status = 0) THEN 'Em edição' :: character varying
        WHEN (c.content_status = 1) THEN 'Aguardando Upload' :: character varying
        WHEN (c.content_status = 2) THEN 'Aguardando Transcode' :: character varying
        WHEN (c.content_status = 3) THEN 'Disponível' :: character varying
        WHEN (c.content_status = 4) THEN 'Associado' :: character varying
        WHEN (c.content_status = 5) THEN 'Excluído' :: character varying
        ELSE NULL :: character varying
    END AS status,
    c."provider",
    c.production_company,
    c.created_on,
    c.duration,
    ccd.tvod,
    ccd.svod
FROM
    (
        postgres_producao.content c
        JOIN postgres_producao.content_country_data ccd ON ((ccd.content_id = c.id))
        AND ccd.id = (SELECT MAX(ccd2.id) 
            FROM postgres_producao.content_country_data ccd2 
            WHERE ccd2.content_id = c.id)
    );